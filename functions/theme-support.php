<?php

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function custom_tiled_gallery_width() {
    return '1200';
}
add_filter( 'tiled_gallery_content_width', 'custom_tiled_gallery_width' );

add_theme_support( 'title-tag' );
add_theme_support( 'responsive-embeds' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'align-wide' );

// Add wp_body_open
if ( ! function_exists( 'wp_body_open' ) ) {
  function wp_body_open() {
      do_action( 'wp_body_open' );
  }
}


// Code Cleanup
remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );



// Disable Author Page
function bearsmith_disable_author_page() {
  global $wp_query;

  if ( is_author() ) {
      $user = get_queried_object();
      $user_url =  get_author_posts_url($user->ID);
      $user_url_modified = str_replace('/author/', '/authors/', $user_url);

      wp_redirect(site_url($user_url_modified), 301); 
      exit; 
  }
}
add_action('template_redirect', 'bearsmith_disable_author_page');




// Customize Excert Link
function wpdocs_excerpt_more( $more ) {
  return '... <a href="'.get_the_permalink().'">Read More</a>';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );



// Move Yoast to bottom
function bearsmith_move_yoast_to_bottom() {
  return 'low';
}
add_filter('wpseo_metabox_prio', 'bearsmith_move_yoast_to_bottom');


add_action( 'pre_get_posts','filter_archive', 1 );
function filter_archive( $query ) {
    if ( ! is_admin() && is_category('3') && $query->is_main_query() )  {
      $query->set('posts_per_page', '8');
      $query->set('category__not_in', array(5));
    }
}


add_filter( 'yoast_seo_development_mode', '__return_true' );


// functions.php
add_filter( 'wpseo_schema_graph_pieces', 'remove_breadcrumbs_from_schema', 11, 2 );

/**
 * Removes the breadcrumb graph pieces from the schema collector.
 *
 * @param array  $pieces  The current graph pieces.
 * @param string $context The current context.
 *
 * @return array The remaining graph pieces.
 */
function remove_breadcrumbs_from_schema( $pieces, $context ) {
    return \array_filter( $pieces, function( $piece ) {
        return ! $piece instanceof \Yoast\WP\SEO\Generators\Schema\Person;
    } );
}