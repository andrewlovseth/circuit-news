<?php

/*
    Register Blocks
*/

add_action('acf/init', 'bearsmith_register_blocks');
function bearsmith_register_blocks() {

    if( function_exists('acf_register_block_type') ) {

        // Related Article
        acf_register_block_type(array(
            'name'              => 'related-article',
            'title'             => __('Related Article'),
            'description'       => __('Select a post to display a preview of inline in an article.'),
            'render_template'   => 'blocks/related-article/related-article.php',
            'category'          => 'layout',
            'icon'              => 'star-filled',
            'align'             => 'full',
        ));

        // DK Intro
        acf_register_block_type(array(
            'name'              => 'dk-intro',
            'title'             => __('Daily Kickoff Intro'),
            'description'       => __('Block for Daily Kickoff Intro'),
            'render_template'   => 'blocks/dk-intro/dk-intro.php',
            'category'          => 'layout',
            'icon'              => 'tagcloud',
            'align'             => 'full',
            'supports'		=> [
                'jsx' 			=> true,
            ]
        ));

        // DK News
        acf_register_block_type(array(
            'name'              => 'dk-news',
            'title'             => __('Daily Kickoff News'),
            'description'       => __('Block for news items in Daily Kickoff'),
            'render_template'   => 'blocks/dk-news/dk-news.php',
            'category'          => 'layout',
            'icon'              => 'format-aside',
            'align'             => 'full',
            'supports'		=> [
                'jsx' 			=> true,
            ]
        ));

        // DK Section
        acf_register_block_type(array(
            'name'              => 'dk-section',
            'title'             => __('Daily Kickoff Section'),
            'description'       => __('Block for sections in Daily Kickoff'),
            'render_template'   => 'blocks/dk-section/dk-section.php',
            'category'          => 'layout',
            'icon'              => 'align-left',
            'align'             => 'full',
            'supports'		=> [
                'jsx' 			=> true,
            ]
        ));

        // DK Banner
        acf_register_block_type(array(
            'name'              => 'dk-banner',
            'title'             => __('Daily Kickoff Banner'),
            'description'       => __('Block for banners in Daily Kickoff'),
            'render_template'   => 'blocks/dk-banner/dk-banner.php',
            'category'          => 'layout',
            'icon'              => 'clipboard',
            'align'             => 'full',
            'supports'		=> [
                'jsx' 			=> true,
            ]
        ));


        // DK Sponsored
        acf_register_block_type(array(
            'name'              => 'dk-sponsored',
            'title'             => __('Daily Kickoff Sponsored'),
            'description'       => __('Block for sponsored content in Daily Kickoff'),
            'render_template'   => 'blocks/dk-sponsored/dk-sponsored.php',
            'category'          => 'layout',
            'icon'              => 'clipboard',
            'align'             => 'full',
            'supports'		=> [
                'jsx' 			=> true,
            ]
        ));

    }
}