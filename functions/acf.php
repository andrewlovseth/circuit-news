<?php



if( function_exists('acf_add_options_page') ) {
    acf_add_options_sub_page('Global');
	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');
}


function bearsmith_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}



// filter for every field
add_filter('acf/fields/relationship/query', 'bearsmith_relationship_query', 10, 3);
add_filter('acf/fields/post_object/query', 'bearsmith_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');