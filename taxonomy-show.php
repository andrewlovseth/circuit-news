<?php

$show = get_queried_object();
$show_art = get_field('show_art', $show);
$show_title = $show->name;
$show_description = $show->description;

//var_dump($show);
get_header(); ?>

	<main class="site-content podcast-template">

		<?php 
			$args = array(
				'show' => $show,
				'show_title' => $show_title
			);
			get_template_part('template-parts/single-podcasts/subscribe', null, $args);           
		?>

		
		<?php 
			$args = array(
				'show' => $show,
				'show_art' => $show_art
			);
			get_template_part('template-parts/taxonomy-show/latest-episode', null, $args);           
		?>		



		<section class="show-details grid">

			<?php 
				$args = array(
					'show' => $show,
					'show_art' => $show_art
				);
				get_template_part('template-parts/taxonomy-show/recent-episodes', null, $args);           
			?>		


            <?php 
                $args = array(
                    'show' => $show,
                    'show_description' => $show_description
                );
                get_template_part('template-parts/single-podcasts/about-the-podcast', null, $args);           
            ?>
		</section>
	</main>

<?php get_footer(); ?>