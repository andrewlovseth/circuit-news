<?php
    global $post; 
    
    $profile = get_field('profile');
    $photo = $profile['photo'];
    $tagline = $profile['tagline'];
    $designation = $profile['designation'];
    $key_cat = $profile['key_category'];

    $editorial = get_field('editorial');
    $link = $editorial['primary_article'];

    $locations = get_the_terms( $post->ID, 'location' );
    $location = $locations[0]->name;

    if($key_cat) {
        $cat = $key_cat->name;
    } else {
        $cats = get_the_terms( $post->ID, 'filters' );
        $cat = $cats[0]->name;
    }
    
?>

<div class="profile <?php echo $designation; ?>">
    <div class="photo">
        <div class="cat">
            <h4><?php echo $cat; ?></h4>
        </div>
        
        <div class="content">
            <a href="<?php the_permalink(); ?>">
                <img src="<?php echo $photo['sizes']['thumbnail']; ?>" alt="<?php echo $photo['alt']; ?>" />
            </a>
        </div>
    </div>

    <div class="info">
        <div class="location">
            <h5><?php echo $location; ?></h5>
        </div>

        <div class="headline name">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        </div>

        <div class="tagline">
            <h4><?php echo $tagline; ?></h4>
        </div>

        <?php if( $link ): ?>
            <div class="profile-link">
                <a href="<?php echo get_permalink($link->ID); ?>"><span>Read:</span> <?php echo get_the_title($link->ID); ?></a>
            </div>
        <?php endif; ?>
    </div>
</div>