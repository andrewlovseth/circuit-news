<?php

/*

	Template used for featured posts

*/

global $theme;
$theme = "overlay";

get_header(); 

?>

	<article <?php post_class('feature page-content'); ?>>

		<?php get_template_part('template-parts/article/featured-image'); ?>

		<section class="header">
			<div class="header-wrapper">

				<?php get_template_part('template-parts/article/title'); ?>

				<?php get_template_part('template-parts/article/dek'); ?>

				<?php get_template_part('template-parts/article/byline'); ?>

				<?php get_template_part('template-parts/article/share'); ?>

				<?php get_template_part('template-parts/article/dateline'); ?>		

			</div>
		</section>

		<?php get_template_part('template-parts/article/body'); ?>

		<?php get_template_part('template-parts/article/footer'); ?>

		<?php // get_template_part('template-parts/article/freewall'); ?>

	</article>
	
<?php get_footer(); ?>