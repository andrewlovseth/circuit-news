<?php
    
    $args = wp_parse_args($args);
    $show = $args['show'];
    $show_description = $args['show_description'];

?>

<div class="about-the-podcast">
    <div class="general">
        <div class="headline section-header">
            <h4>About the Podcast</h4>
        </div>

        <div class="copy p3">
            <p><?php echo $show_description; ?></p>
        </div>
    </div>

    <div class="hosts">
        <div class="headline section-header">
            <h4>Hosts</h4>
        </div>

        <?php if(have_rows('hosts', $show)): while(have_rows('hosts', $show)): the_row(); 
            $host = get_sub_field('host');
            $name = $host['name'];
            $bio = $host['bio'];
            $photo = $host['photo'];
        ?>
            
            <div class="host">
                <div class="photo">
                    <img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
                </div>

                <div class="info">
                    <div class="headline name">
                        <h5><?php echo $name; ?></h5>
                    </div>

                    <div class="copy bio p3">
                        <?php echo $bio; ?>
                    </div>                            
                </div>                            
            </div>

        <?php endwhile; endif; ?>
    </div>

</div>