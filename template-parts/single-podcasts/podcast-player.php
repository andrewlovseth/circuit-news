<section class="podcast-player grid">
    <div class="embed">
        <?php the_field('player_embed'); ?>
    </div>
</section>