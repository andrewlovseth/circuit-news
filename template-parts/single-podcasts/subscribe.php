<?php
    
    $args = wp_parse_args($args);
    $show = $args['show'];
    $show_title = $args['show_title'];
    $show_link = $args['show_link'];

?>

<section class="podcast-subscribe grid">
    <div class="headline">
        <h4><a href="<?php echo $show_link; ?>"><?php echo $show_title; ?></a></h4>
    </div>

    <div class="links">
        <?php if(have_rows('subscribe_links', $show)): while(have_rows('subscribe_links', $show)): the_row(); ?>

            <?php
                $icon = get_sub_field('icon');
                $link = get_sub_field('link');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <div class="link">
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                        <span class="icon"><img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" /></span>
                        <span class="title"><?php echo esc_html($link_title); ?></span>
                    </a>
                </div>

            <?php endif; ?>

        <?php endwhile; endif; ?>
    </div>
</section>