<section class="episode-header grid">
    <div class="meta">
        <span class="time"><?php the_time('M j, Y · g:i a'); ?></span>
    </div>
    
    <div class="headline">
        <h3><?php the_title(); ?></h3>
    </div>
    
    <div class="dek copy p3">
        <p><?php the_field('dek'); ?></p>
    </div>

    <div class="share">
        <div class="link">
            <a href="https://www.facebook.com/sharer.php?u=<?php echo esc_html(get_permalink()); ?>" class="facebook" target="_blank">
                <img src="<?php bloginfo('template_directory'); ?>/images/facebook-icon-blue.svg" alt="Facebook" />
            </a>
        </div>

        <div class="link">
            <a href="https://twitter.com/intent/tweet?text=<?php echo esc_html(get_the_title()); ?>&url=<?php echo esc_html(get_permalink()); ?>" class="twitter" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/twitter-icon-blue.svg" alt="Twitter" /></a>
        </div>
    </div>
</section>


