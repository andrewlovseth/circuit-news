<div class="show-notes body p1">
    <div class="headline">
        <h3>Show Notes</h3>
    </div>

    <div class="copy">
        <?php the_field('show_notes'); ?>
    </div>
</div>