<?php
    
    $args = wp_parse_args($args);
    $show_link = $args['show_link'];
    $show_title = $args['show_title'];

?>

<section class="show-header grid">
    <div class="info">
        <div class="headline">
            <h2><a href="<?php echo $show_link; ?>"><?php echo $show_title; ?></a></h2>
        </div>

        <div class="meta">
            <span class="time"><?php the_time('M j, Y · g:i a'); ?></span>
        </div>
    </div>
</section>