<?php

	$home = get_option('page_on_front');
	$preview_type = get_field('preview_type', $home);
	$preview_photo = get_field('preview_photo', $home);
	
?>

<section id="subscribe" class="subscribe page-content">

	<?php if(is_front_page()): ?>
		<?php get_template_part('template-parts/home/banner'); ?>
	<?php endif; ?>

	<div class="info">
		<div class="headline">
			<h1 class="x-large-title"><?php the_field('subscribe_headline', $home); ?></h1>
		</div>

		<div class="dek">
			<p><?php the_field('subscribe_dek', $home); ?></p>
		</div>

		<?php get_template_part('template-parts/global/subscribe-form'); ?>

		<div class="pullquote">
			<blockquote>
				<p><?php the_field('subscribe_quote', $home); ?></p>
			</blockquote>

			<cite>
				<?php $image = get_field('subscribe_quote_photo', $home); if( $image ): ?>
					<div class="photo">
						<?php echo wp_get_attachment_image($image['ID'], 'large'); ?>
					</div>
				<?php endif; ?>

				<div class="source">
					<h4><?php the_field('subscribe_quote_name', $home); ?></h4>
					<h5><?php the_field('subscribe_quote_details', $home); ?></h5>
				</div>
			</cite>
		</div>					
	</div>

	<div class="preview">
		<?php if($preview_type == 'photo'): ?>
			<div class="photo">
				<?php if( $preview_photo ): ?>
					<?php echo wp_get_attachment_image($preview_photo['ID'], 'full'); ?>
				<?php endif; ?>
			</div>
		<?php else: ?>
			<div class="frame">
				<div class="email">
					<div class="iframe-container">
						<?php if(get_field('mobile_preview', 'options')): ?>
							<iframe src="<?php the_field('mobile_preview', 'options'); ?>" loading="lazy" width="375" height="740"></iframe>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

	</div>
</section>