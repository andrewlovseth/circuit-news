<?php if(get_post(get_post_thumbnail_id())->post_content): ?>

	<div class="credit">
		<p><?php echo get_post(get_post_thumbnail_id())->post_content; ?></p>
	</div>

<?php endif; ?>