<nav class="main-nav">
	<div class="nav-wrapper">

		<div class="info">
			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo_white_alt', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="navigation">
				<div class="links">
					
					<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>

						<?php 
							$link = get_sub_field('link');
							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>

							<div class="link">
								<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
							</div>

						<?php endif; ?>


					<?php endwhile; endif; ?>

					<div class="link subscribe-link">
						<a href="#" class="subscribe-trigger">Subscribe</a>
					</div>
				</div>
			</div>

			<div class="social">
				<div class="links">

					<?php if(have_rows('social', 'options')): while(have_rows('social', 'options')): the_row(); ?>

						<?php
							$light_icon = get_sub_field('light_icon');
							$link = get_sub_field('link');

							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>

							<div class="social-link <?php echo sanitize_title_with_dashes($link_title); ?>">
								<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
									<img src="<?php echo $light_icon['url']; ?>" alt="<?php echo $link_title; ?>" />
								</a>
							</div>

						<?php endif; ?>

					<?php endwhile; endif; ?>

				</div>
			</div>
		
		</div>
		
	</div>
</nav>