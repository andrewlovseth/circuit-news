<?php 

    global $theme;

?>

<div class="social">

    <?php if(have_rows('social', 'options')): while(have_rows('social', 'options')): the_row(); ?>

        <?php
            $light_icon = get_sub_field('light_icon');
            $dark_icon = get_sub_field('dark_icon');
            $link = get_sub_field('link');

            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

            <div class="social-link <?php echo sanitize_title_with_dashes($link_title); ?>">
                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                    <?php if($theme == 'overlay'): ?>
                        <img src="<?php echo $light_icon['url']; ?>" alt="<?php echo $link_title; ?>" />
                    <?php else: ?>
                        <img src="<?php echo $dark_icon['url']; ?>" alt="<?php echo $link_title; ?>" />
                    <?php endif; ?>
                </a>
            </div>

        <?php endif; ?>

    <?php endwhile; endif; ?>
				
</div>