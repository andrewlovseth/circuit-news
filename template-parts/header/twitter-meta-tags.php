<?php if(is_singular('post')): ?>
    
    <?php
        $image = get_the_post_thumbnail_url(get_the_ID(), 'large'); 
        $dek = get_field('dek');
    ?>

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="<?php echo $image; ?>" />
    <meta name="twitter:description" content="<?php echo $dek; ?>" />

<?php endif; ?>

<?php if (is_singular('podcasts') ): 
    $show_array = wp_get_post_terms( $post->ID, 'show', array( 'fields' => 'all' ) );
    $show = $show_array[0];
    $dek = get_field('dek');    

    if(get_field('show_art')) {
        $show_art = get_field('show_art');
    } else {
        $show_art = get_field('show_art', $show);
    }
?>
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="<?php echo $show_art['sizes']['large']; ?>" />
    <meta name="twitter:description" content="<?php echo $dek; ?>" />
<?php endif; ?>

