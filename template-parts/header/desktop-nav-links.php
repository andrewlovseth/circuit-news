<div class="desktop-nav-links">
    <?php if(have_rows('desktop_nav_links', 'options')): while(have_rows('desktop_nav_links', 'options')): the_row(); ?>

        <?php 
            $link = get_sub_field('link');
            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

            <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span></a>

        <?php endif; ?>

    <?php endwhile; endif; ?>
</div>