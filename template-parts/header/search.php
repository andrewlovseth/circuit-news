<?php 

    global $theme;

?>

<div class="search">
    <a href="#" class="search-btn search-trigger">
        <?php if($theme == 'overlay'): ?>
            <img src="<?php bloginfo('template_directory') ?>/images/search-icon.svg" alt="Search" />
        <?php else: ?>
            <img src="<?php bloginfo('template_directory') ?>/images/search-icon-black.svg" alt="Search" />
        <?php endif; ?>
    </a>
</div>