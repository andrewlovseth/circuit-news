<section class="page-header">
    <div class="wrapper">

        <div class="meta">
            <div class="category">
                <span><?php the_field('category'); ?></span>
            </div>

            <div class="location">
                <span><?php the_field('location'); ?></span>
            </div>
        </div>

        <div class="headline">
            <h1 class="x-large-title"><?php the_title(); ?></h1>
        </div>

    </div>
</section>