<section class="main">
    <div class="wrapper">
    
        <section class="content copy p2">
            <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; endif; ?>
        </section>

    </div>
</section>