<section class="page-header">
    <div class="wrapper">

        <div class="headline">
            <h1 class="x-large-title"><?php the_title(); ?></h1>
        </div>

    </div>
</section>