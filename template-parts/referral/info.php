<div class="info">
    <div class="headline">
        <h1 class="x-large-title"><?php the_field('referral_headline'); ?></h1>
    </div>

    <div class="dek">
        <p><?php the_field('referral_deck'); ?></p>
    </div>

    <div class="form">
        <?php 
            $shortcode = get_field('referral_form_shortcode');
            echo do_shortcode($shortcode);
        ?>

        <div class="form-note">
            <p><?php the_field('referral_form_note'); ?></p>
        </div>
    </div>

    <div class="pullquote">
        <blockquote>
            <p><?php $home = get_option('page_on_front'); the_field('subscribe_quote', $home); ?></p>
        </blockquote>

        <cite>
            <div class="photo">
                <img src="<?php $image = get_field('subscribe_quote_photo', $home); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>

            <div class="source">
                <h4><?php the_field('subscribe_quote_name', $home); ?></h4>
                <h5><?php the_field('subscribe_quote_details', $home); ?></h5>
            </div>
        </cite>
    </div>					
</div>