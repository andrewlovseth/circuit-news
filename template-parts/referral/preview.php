<div class="preview">
    <div class="frame">
        <div class="email">
            <div class="iframe-container">
                <?php if(get_field('mobile_preview', 'options')): ?>
                    <iframe src="<?php the_field('mobile_preview', 'options'); ?>"></iframe>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>