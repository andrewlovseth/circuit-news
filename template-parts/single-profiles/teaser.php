<?php

    $args = wp_parse_args($args);
    $link = $args['link'];

?>

<article class="profile-teaser">

    <div class="details">
        <div class="title">
            <h3><a href="<?php echo get_permalink($link->ID); ?>"><?php echo get_the_title($link->ID); ?></a></h3>
        </div>

        <div class="byline">
            <?php $authors = get_field('authors', $link->ID); if( $authors ): ?>
                <div class="authors">
                    <span class="by">By</span>
                    <?php foreach( $authors as $a ): ?>
                        <div class="author">
                            <a href="<?php echo get_permalink( $a->ID ); ?>"><?php echo get_the_title( $a->ID ); ?></a>
                        </div>							
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>						

            <div class="date">
                <span><?php the_time('F j, Y'); ?></span>
            </div>					
        </div>

        <?php if(get_field('dek', $link->ID)): ?>

            <div class="dek">
                <p><?php the_field('dek', $link->ID); ?></p>
            </div>

        <?php endif; ?>
    </div>
</article>