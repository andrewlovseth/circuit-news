<?php

    $profiles = get_posts(
        array(
            'posts_per_page' => -1,
            'post_type' => 'profiles',
            'orderby' => 'title',
            'order' => 'ASC'
        )
    );

    $profile_ids = wp_list_pluck( $profiles, 'ID' );
    $profiles_count = count($profile_ids);
    $current_index = array_search($post->ID, $profile_ids);
    
    // Prev Index
    if($current_index == 0) {
        $prev_index = $profiles_count - 1;
    } else {
        $prev_index = $current_index - 1;
    }

    // Next Index
    if($current_index == $profiles_count - 1) {
        $next_index = 0;
    } else {
        $next_index = $current_index + 1;
    }

    $prev_id = $profile_ids[$prev_index];
    $next_id = $profile_ids[$next_index];

    $prev_locations = get_the_terms( $prev_id, 'location' );
    $prev_location = $prev_locations[0]->name;
    $prev_tagline  = get_field('profile_tagline', $prev_id);
    $prev_cats = get_the_terms( $prev_id, 'filters' );
    $prev_cat = $prev_cats[0]->name;

    $next_locations = get_the_terms( $next_id, 'location' );
    $next_location = $next_locations[0]->name;
    $next_tagline  = get_field('profile_tagline', $next_id);
    $next_cats = get_the_terms( $next_id, 'filters' );
    $next_cat = $next_cats[0]->name;
    

?>

<section class="pagination grid">
    <div class="prev link">
        <a href="<?php echo get_permalink($prev_id); ?>" id="prev">
            <span class="location"><?php echo $prev_cat; ?></span>
            <span class="name"><?php echo get_the_title($prev_id); ?></span>
            <span class="tagline"><?php echo $prev_tagline; ?></span>
        </a>
    </div>

    <div class="next link">
        <a href="<?php echo get_permalink($next_id); ?>" id="next">
            <span class="location"><?php echo $next_cat; ?></span>
            <span class="name"><?php echo get_the_title($next_id); ?></span>
            <span class="tagline"><?php echo $next_tagline; ?></span>
        </a>
    </div>
</section>