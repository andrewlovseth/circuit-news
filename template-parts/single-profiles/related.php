<section class="related grid">
    <div class="section-header">
        <h3>Related Profiles</h3>
    </div>

    <div class="profiles-grid">
        <?php
            $profile = get_field('profile');
            $key_cat = $profile['key_category'];

            if($key_cat) {
                $cat = $key_cat->term_id;
            } else {
                $cats = get_the_terms( $post->ID, 'filters' );
                $cat = $cats[0]->term_id;
            }


            $locations = get_the_terms( $post->ID, 'location' );
            $location = $locations[0];

            $profile = get_field('profile');
            $designation = $profile['designation'];

            $include_ids = array();

            // Get posts in same Cat/Filter
            $cat_posts = get_posts(
                array(
                    'posts_per_page' => -1,
                    'post_type' => 'profiles',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'filters',
                            'field' => 'term_id',
                            'terms' => $cat,
                        )
                    )
                )
            );

            foreach($cat_posts as $cat_post) {
                array_push($include_ids, $cat_post->ID);
            }


            // Remove this profile
            $include_ids = array_diff($include_ids, array($post->ID));

            // If not 4, get posts in same Location
            if(count($include_ids) < 4) {
                $location_posts = get_posts(
                    array(
                        'posts_per_page' => -1,
                        'post_type' => 'profiles',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'location',
                                'field' => 'term_id',
                                'terms' => $location->term_id,
                            )
                        )
                    )
                );

                foreach($location_posts as $location_post) {
                    array_push($include_ids, $location_post->ID);
                }
    
                // Remove this profile
                $include_ids = array_diff($include_ids, array($post->ID));
            }

            // If not 4, gets posts from same designation
            if(count($include_ids) < 4) {
                $designation_posts = get_posts(
                    array(
                        'posts_per_page' => -1,
                        'post_type' => 'profiles',
                        'meta_query' => array(
                            array(
                                'key'     => 'profile_designation',
                                'value'   => $designation,
                                'compare' => '=',
                            ),
                        ),
                    )
                );

                foreach($designation_posts as $designation_post) {
                    array_push($include_ids, $designation_post->ID);
                }
    
                // Remove this profile
                $include_ids = array_diff($include_ids, array($post->ID));
            }


            $args = array(
                'post_type' => 'profiles',
                'post__in' => $include_ids,
                'post__not_in' => $exclude_ids,
                'posts_per_page' => 4,
                'orderby' => 'rand'
            );

            $query = new WP_Query( $args );
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

            <?php get_template_part('alm_templates/profile'); ?>

        <?php endwhile; endif; wp_reset_postdata(); ?>
    </div>
</section>