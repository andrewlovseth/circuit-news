<section class="main">
    <div class="wrapper">
    
        <section class="content copy p2">
            <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; endif; ?>
        </section>

        <section class="masthead">
            <div class="section-header">
                <h3>Masthead</h3>
            </div>

            <?php $posts = get_field('masthead'); if( $posts ): ?>
                <?php foreach( $posts as $p ): ?>
                    <div class="position">
                        <a href="<?php echo get_permalink( $p->ID ); ?>">
                            <span class="name"><?php echo get_the_title( $p->ID ); ?></span>
                            <span class="title"><?php the_field('title', $p->ID); ?></span>
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </section>

    </div>
</section>