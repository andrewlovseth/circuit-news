<?php			
	$image = get_post_thumbnail_id($p->ID);
?>

<section class="featured-image">
	<div class="content">
		<?php echo wp_get_attachment_image($image, 'large'); ?>
	</div>

	<?php get_template_part('template-parts/global/photo-credit'); ?>
</section>