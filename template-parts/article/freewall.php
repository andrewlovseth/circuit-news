<?php

	$freewall = get_field('freewall', 'options');
	$headline = $freewall['headline'];
	$copy = $freewall['copy'];

?>

<div class="pico-freewall-message">
    <div class="container">

        <div class="logo">
            <img src="<?php $image = get_field('logo_white', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </div>

        <div class="info">

            <?php if($headline): ?>
                <div class="headline">
                    <h3><?php echo $headline; ?></h3>
                </div>
            <?php endif; ?>

            <?php if($copy): ?>
                <div class="copy p3">
                    <?php echo $copy; ?>
                </div>
            <?php endif; ?>

            <div class="subscribe">
                <a href="#" class="subscribe-trigger subscribe-btn">Subscribe</a>
            </div>

        </div>
        
    </div>
</div>