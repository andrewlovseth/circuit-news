<?php
  
  $current_url = "https://".$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $thumbnail = get_the_post_thumbnail_url(get_the_ID(), 'thumbnail'); 
  $image = get_the_post_thumbnail_url(get_the_ID(), 'large'); 
  $authors = get_field('authors');
  $authors_display = "";
  if( $authors ) {
    $i = 1;
    foreach( $authors as $a ) {
      if($i > 1) {
        $authors_display .= ', ';
      }

      $authors_display .= get_the_title( $a->ID );

      $i++;
    }
  }

  $logo = get_field('logo_color', 'options');
  $logo_url = $logo['url'];

  if(get_field('dek')) {
    $dek = get_field('dek');
  } else {
    $dek = wp_trim_words( get_the_content(), 20 );
  }

?>

    <script type="application/ld+json">
      {
        "@context": "https://schema.org",
        "@type": "NewsArticle",
        "mainEntityOfPage": {
          "@type": "WebPage",
          "@id": "<?php echo $current_url; ?>"
        },
        "headline": "<?php the_title(); ?>",
        "image": [
          "<?php echo $image; ?>",
          "<?php echo $thumbnail; ?>"
         ],
        "datePublished": "<?php the_time('c'); ?>",
        "dateModified": "<?php the_modified_time('c'); ?>",
        "author": {
          "@type": "Person",
          "name": "<?php echo $authors_display; ?>"
        },
         "publisher": {
          "@type": "Organization",
          "name": "Jewish Insider",
          "logo": {
            "@type": "ImageObject",
            "url": "<?php echo $logo_url; ?>"
          }
        },
        "description": "<?php echo esc_html($dek); ?>"
      }
    </script>

