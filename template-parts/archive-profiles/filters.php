<div class="sidebar">
    <div class="filters">

        <div class="sidebar-header">
            <h4>Filters</h4>

            <div class="mobile-toggle-filters">
                <a href="#" class="js-filter-toggle">[ Toggle ]</a>
            </div>
        </div>

        <?php
            $filter_array = array(
                "id" =>  "profiles",
                "style" =>  "change",
                "reset_button" =>  false,
                "date_created" =>  1615914178,
                "date_modified" =>  1615917221,
                "filters" =>  array(
                    array(
                        "key" =>  "taxonomy",
                        "field_type" =>  "select",
                        "taxonomy" =>  "filters",
                        "taxonomy_operator" =>  "IN",
                        "default_select_option" =>  "-- Select --",
                        "title" =>  "Category",
                        "show_count" =>  false,
                        "section_toggle" =>  false
                    ),
                    array(
                        "key" =>  "taxonomy",
                        "field_type" =>  "select",
                        "taxonomy" =>  "location",
                        "taxonomy_operator" =>  "IN",
                        "default_select_option" =>  "-- Select -- ",
                        "title" =>  "Location",
                        "show_count" =>  false,
                        "section_toggle" =>  false
                    ),
                    array(
                        "key" =>  "meta",
                        "field_type" =>  "checkbox",
                        "values" =>  array(
                            array(
                                "label" =>  "Emerging",
                                "value" =>  "emerging"
                            ),
                            array(
                                "label" =>  "Established",
                                "value" =>  "established"
                            )
                        ),
                        "title" =>  "Status",
                        "show_count" =>  false,
                        "section_toggle" =>  false,
                        "meta_operator" =>  "IN",
                        "meta_type" =>  "CHAR",
                        "meta_key" =>  "profile_designation"
                    )
                )
            );

            echo alm_filters($filter_array, 'profiles-grid');
        ?>

    </div>
</div>