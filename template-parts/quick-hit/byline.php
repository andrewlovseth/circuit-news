	<div class="byline">
		<?php $authors = get_field('authors'); if( $authors ): ?>
			<div class="authors">
				<span class="by">By</span>
				<?php foreach( $authors as $a ): ?>
					<div class="author">
						<a href="<?php echo get_permalink( $a->ID ); ?>"><?php echo get_the_title( $a->ID ); ?></a>
					</div>							
				<?php endforeach; ?>
			</div>
		<?php endif; ?>			

		<div class="date">
			<span><?php the_time('F j, Y'); ?></span>
		</div>	
	</div>