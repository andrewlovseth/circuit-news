<?php $image = get_post_thumbnail_id(); if( $image ): ?>

	<section class="quick-hit-photo">
		<div class="photo-wrapper">
			<?php echo wp_get_attachment_image($image, 'large'); ?>
			
			<?php get_template_part('template-parts/global/photo-credit'); ?>
		</div>

		<?php if(get_the_post_thumbnail_caption()): ?>
			<div class="caption">
				<p><?php echo get_the_post_thumbnail_caption(); ?></p>
			</div>
		<?php endif; ?>	
	</section>

<?php endif; ?>