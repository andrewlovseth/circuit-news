<section class="newsletter-archive">
    <div class="wrapper">

        <div class="section-header">
            <h2>Recent Weekly Circuit Newsletters</h2>
        </div>

        <div class="recent">					
            <?php
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 8,
                    'cat' => 8,
                    'order' => 'DESC'
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

                <article class="archive-post">
                    <div class="timestamp">
                        <a href="<?php the_permalink(); ?>">
                            <span class="day"><?php the_time('D'); ?>, </span><span class="date"><?php the_time('M j'); ?></span>
                        </a>
                    </div>

                    <?php $image = get_post_thumbnail_id(); if( $image ): ?>
                        <div class="photo">
                            <div class="content">
                                <a href="<?php the_permalink(); ?>">
                                    <?php echo wp_get_attachment_image($image, 'thumbnail'); ?>
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="headline">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </div>
                </article>

            <?php endwhile; endif; wp_reset_postdata(); ?>
        </div>

    </div>
</section>