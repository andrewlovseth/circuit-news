<?php

	$coming_soon = get_field('coming_soon', 'options');
	$headline = $coming_soon['headline'];
	$copy = $coming_soon['copy'];

if(is_category('daily-circuit')): ?>

    <section class="coming-soon">

        <div class="info">
            <?php if($headline): ?>
                <div class="headline">
                    <h3><?php echo $headline; ?></h3>
                </div>
            <?php endif; ?>

            <?php if($copy): ?>
                <div class="copy p3 extended">
                    <?php echo $copy; ?>
                </div>
            <?php endif; ?>

            <div class="subscribe-container">
                <a href="#" class="subscribe-trigger subscribe-btn clear-charcoal">Subscribe</a>
            </div>
        </div>



    </section>

<?php endif; ?>