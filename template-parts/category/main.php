<section class="main">
    <div class="wrapper">

        <?php if ( have_posts() ): $count = 1; ?>

            <section class="posts">

                <?php while ( have_posts() ): the_post(); ?>
                    <article class="post-<?php echo $count; ?>">
                        <div class="photo">
                            <div class="content">
                                <a href="<?php the_permalink(); ?>">
                                    <?php $image = get_post_thumbnail_id(); echo wp_get_attachment_image($image, 'large'); ?>
                                </a>								
                            </div>
                        </div>

                        <div class="info">
                            <div class="meta">
                                <span class="date"><?php the_time('F j, Y'); ?></span>
                            </div>
                            
                            <div class="headline">
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            </div>

                            <?php if($count == 1): ?>
                                <div class="copy p2">
                                    <p><?php the_field('dek'); ?></p>
                                </div>
                            <?php endif; ?>
                        </div>
                    </article>

                <?php $count++; endwhile; ?>

            </section>

            <?php
                the_posts_pagination(
                    array(
                        'mid_size'  => 1,
                        'prev_text' => __('Prev'),
                        'next_text' => __('Next'),
                    )
                );
            ?>

        <?php else: ?>
            
            <?php get_template_part('template-parts/category/coming-soon'); ?>

        <?php endif; ?>

    </div>
</section>