<section class="featured-articles">
	<?php $posts = get_field('featured_articles'); if( $posts ): ?>
		<?php foreach( $posts as $p ): ?>

			<?php			
				$image = get_post_thumbnail_id($p->ID);
				$thumbnail = get_the_post_thumbnail_url( $p->ID, 'thumbnail' );
				$alt = get_post_meta($p->ID, '_wp_attachment_image_alt', true);
			?>

			<article data-thumb="<?php echo $thumbnail; ?>" data-alt="<?php echo $alt; ?>">
				<a href="<?php echo get_permalink($p->ID); ?>">
					<div class="photo">
						<div class="content">
							<?php if( $image ): ?>
								<?php echo wp_get_attachment_image($image, 'large'); ?>
							<?php endif; ?>
						</div>
					</div>
				
					<div class="info">
						<div class="info-wrapper">
							<div class="headline">
								<h1 class="x-large-title">
									<?php echo get_the_title($p->ID); ?>
								</h1>
							</div>
						
							<div class="dek">
								<p><?php the_field('dek', $p->ID); ?></p>
							</div>
						</div>
					</div>
				</a>
			</article>

		<?php endforeach; ?>
	<?php endif; ?>
</section>