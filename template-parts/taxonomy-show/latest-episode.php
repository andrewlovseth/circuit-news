<?php
    
    $args = wp_parse_args($args);
    $show = $args['show'];
    $show_art = $args['show_art'];

?>

<div class="latest-episode grid">
    
    <?php
        $args = array(
            'post_type' => 'podcasts',
            'tax_query' => array(
                array(
                    'taxonomy' => 'show',
                    'field'    => 'slug',
                    'terms'    => $show->slug,
                ),
            ),
            'posts_per_page' => 1
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

        <div class="section-header headline">
            <h3>Latest Episode</h3>
            <a href="<?php the_permalink(); ?>" class="show-notes-link">(Show Notes)</a>
        </div>
    
        <div class="podcast-player">
            <div class="embed">
                <?php the_field('player_embed'); ?>
            </div>
        </div>

    <?php endwhile; endif; wp_reset_postdata(); ?>				
</div>