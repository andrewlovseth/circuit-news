<?php
    
    $args = wp_parse_args($args);
    $show = $args['show'];
    $show_art = $args['show_art'];
    $show_slug = $show->slug;

?>

<div class="recent-episodes">
    <div class="section-header headline">
        <h3>Recent Episodes</h3>
    </div>

    <div class="show-list">    
        <?php echo do_shortcode('[ajax_load_more id="podcast-episodes" container_type="div" theme_repeater="podcast-episode.php" post_type="podcasts" posts_per_page="12" taxonomy="show" taxonomy_terms="' . $show_slug . '" taxonomy_operator="IN" scroll="false"]'); ?>
    </div>
</div>