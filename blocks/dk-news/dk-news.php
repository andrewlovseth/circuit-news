<?php

/*
 * Daily Kickoff News Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'dk-news-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'dk-news dk-block wp-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$linked_post = get_field('post');
$permalink = get_permalink($linked_post->ID);

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="tagline">
        <h4><?php the_field('tagline'); ?></h4>
    </div>

    <div class="headline" data-url="<?php echo esc_attr($permalink); ?>">
        <h3><?php the_field('headline'); ?></h3>
    </div>

    <div class="content">
        <InnerBlocks />
    </div>

    <?php if(!$is_preview): ?>
        <div class="share">
            <div class="share-wrapper">
                <span class="label">Share</span>

                <div class="link facebook">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink($linked_post->ID); ?>" rel="external">
                        <img src="<?php bloginfo('template_directory') ?>/images/facebook-icon.png" alt="Facebook">
                    </a>
                </div>

                <div class="link twitter">
                    <a href="https://twitter.com/intent/tweet/?text=<?php echo get_the_title($linked_post->ID); ?>+<?php echo get_permalink($linked_post->ID); ?>" rel="external">
                        <img src="<?php bloginfo('template_directory') ?>/images/twitter-icon.png" alt="Twitter">
                    </a>
                </div>

                <div class="link email">
                    <a href="mailto:?subject=Jewish Insider: <?php echo get_the_title($linked_post->ID); ?>&body=<?php echo get_the_title($linked_post->ID); ?>%0D%0A<?php echo get_permalink($linked_post->ID); ?>">
                        <img src="<?php bloginfo('template_directory') ?>/images/email-icon.png" alt="Email">
                    </a>
                </div>
            </div>

        </div>
    <?php endif; ?>
    
</section>