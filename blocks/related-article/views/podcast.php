<?php 

$args = wp_parse_args($args);
$related_post = $args['post'];

$query_args = array(
    'post_type' => 'podcasts',
    'p' => $related_post->ID
);
$query = new WP_Query( $query_args );
if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); 




$show_array = wp_get_post_terms( $post->ID, 'show', array( 'fields' => 'all' ) );
$show = $show_array[0];
$show_title = $show->name;
$show_description = $show->description;
$show_link = get_term_link($show->slug, 'show');

if(get_field('show_art',  $related_post->ID)) {
    $image = get_field('show_art', $related_post->ID);
} else {
    $image = get_field('show_art', $show);
}



?>

	<div class="related-content related-podcast">
        <a href="<?php the_permalink(); ?>" class="related-link">
            <?php if($image): ?>
                <div class="photo">
                    <div class="content">                    
                        <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>
                </div>
            <?php endif; ?>

            <div class="info">
                <?php if($show_title): ?>
                    <div class="tagline">
                        <h4><?php echo $show_title; ?></h4>
                    </div>
                <?php endif; ?>

                <div class="headline">
                    <h3><?php the_title(); ?></h3>
                </div>

                <div class="read-more">
                    <span class="label">Listen</span>           
                </div>
            </div>
        </a>
	</div>

<?php endwhile; endif; wp_reset_postdata(); ?>

