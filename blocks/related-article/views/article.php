<?php 

$args = wp_parse_args($args);
$related_post = $args['post']; 

$query_args = array(
    'p' => $related_post->ID
);
$query = new WP_Query( $query_args );
if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); 

$image = get_post_thumbnail_id($related_post->ID);
$tagline = get_field('tagline', $related_post->ID);

?>

	<div class="related-article related-content">
        <a href="<?php the_permalink(); ?>" class="related-link">
            <?php if($image): ?>
                <div class="photo">
                    <div class="content">
                        <?php echo wp_get_attachment_image($image, 'thumbnail'); ?>
                    </div>
                </div>
            <?php endif; ?>

            <div class="info">
                <?php if($tagline): ?>
                    <div class="tagline">
                        <h4><?php echo $tagline; ?></h4>
                    </div>
                <?php endif; ?>

                <div class="headline">
                    <h3><?php the_title(); ?></h3>
                </div>

                <div class="read-more">
                    <span class="label">Read more</span>            
                </div>
            </div>
        </a>
	</div>

<?php endwhile; endif; wp_reset_postdata(); ?>

