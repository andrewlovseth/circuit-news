<?php

/*
 * Daily Kickoff Banner Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'dk-banner-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'dk-banner dk-block wp-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$image = get_field('image');
$link = get_field('link');

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="content">
        <a href="<?php echo esc_attr($link); ?>" target="_blank">
            <?php if( $image ): ?>
                <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
            <?php endif; ?>
        </a>        
    </div>    
</section>