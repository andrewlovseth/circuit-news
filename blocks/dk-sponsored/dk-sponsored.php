<?php

/*
 * Daily Kickoff Sponsored Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'dk-sponsored-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'dk-sponsored dk-block wp-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$sponsored_label = get_field('sponsored_label');
$logo = get_field('logo');
$background_color = get_field('background_color');
$content = get_field('content');


?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>"<?php if($background_color): ?> style="background-color: <?php echo $background_color; ?>"<?php endif; ?>>
    <div class="content">
            
        <?php if( $sponsored_label ): ?>
            <div class="sponsored-label">
                <h5><?php echo $sponsored_label; ?></h5>
            </div>
        <?php endif; ?>
        
        <?php if( $logo ): ?>
            <div class="logo">
                <?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>
            </div>
        <?php endif; ?>
    
        <div class="copy">
            <?php echo $content; ?>
        </div>

    </div>    
</section>