(function ($, window, document, undefined) {

	$(document).ready(function($) {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});

		// Smooth Scroll
		$('.smooth').smoothScroll();


		// Menu Toggle
		$('#toggle').click(function(){
			$('header, nav, #page').toggleClass('open');
			return false;
		});

		// Header, Nav, #page Toggle
		$('#page').on('click', function(){
			$('header, nav, #page').removeClass('open');
		});



		// Subscribe Trigger
		$('.subscribe-trigger').click(function(){
			$('body').toggleClass('subscribe-open');
			return false;
		});


		// Subscribe Close
		$('.subscribe-close').click(function(){
			$('body').toggleClass('subscribe-open');
			return false;
		});


		// Search Trigger
		$('.search-trigger').click(function(){
			$('body').toggleClass('search-open');
			return false;
		});


		// Search Close
		$('.search-close').click(function(){
			$('body').toggleClass('search-open');
			return false;
		});


		// Email Gateway Close
		$('.email-gateway-close').click(function(){
			$('body').toggleClass('email-gateway-on');
			return false;
		});

		$('#mailchimp-gateway').submit(function(){
			var mailchimpform = $(this);
			$.ajax({
				url:mailchimpform.attr('action'),
				type:'POST',
				data:mailchimpform.serialize(),
				success:function(data){
					alert(data);
				}
			});
			return false;
		});


		// Homepage Carousel
		$('.page-template-homepage .featured-articles').slick({
			dots: true,
			arrows: false,
			infinite: true,
			speed:  500,
			autoplay: true,
			autoplaySpeed: 6000,
			slidesToShow: 1,
			slidesToScroll: 1,
			customPaging : function(slider, i) {
				var thumb = $(slider.$slides[i]).data('thumb');
				var alt = $(slider.$slides[i]).data('alt');
				return '<img src="' + thumb + '" alt="' + alt + '" />';
			},

		});


		// New User Popup
		var showPopup = localStorage.getItem('showPopup');
		if (showPopup == null) {
			localStorage.setItem('showPopup', 1);

			// Show popup here
			$('#new-user').fadeIn('slow');
		}

		$('.new-user-close-btn').on('click', function(){
			$('#new-user').fadeOut('slow');
			return false;
		});



		$('.combo-box select').on('change', function () {
			var url = $(this).val();
			if (url) {
				window.location = url;
			}
			return false;
		});


		$('.js-filter-toggle').on('click', function(){
			$('.alm-filters-container').toggleClass('active');
			
			return false;
		});


		$('.show-more-btn .btn').on('click', function(){
			var article = jQuery(this).closest('section.body');
			jQuery(article).addClass('expanded');

			return false;
		});

	});

	$(document).keydown(function(e) {	
		// ESC
		if (e.keyCode == 27) {
			jQuery('header, nav, #page').removeClass('open');
		}
		
		if(jQuery('body').hasClass('single-profiles')) {
			// Left Arrow
			if (e.keyCode == 37) {
				var prev_href = jQuery('#prev').attr('href');
				window.location.href = prev_href;
			}

			// Right Arrow
			if (e.keyCode == 39) {
				var next_href = jQuery('#next').attr('href');
				window.location.href = next_href;
			}
		}
	});

})(jQuery, window, document);