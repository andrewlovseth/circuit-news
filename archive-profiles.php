<?php get_header(); ?>

    <main class="site-content grid">
        <section class="page-header">
            <a href="<?php echo site_url('/profiles/'); ?>">
                <img src="<?php bloginfo('template_directory'); ?>/images/profiles-header.png" alt="Profiles" />
            </a>
        </section>

        <?php get_template_part('template-parts/archive-profiles/filters'); ?>

        <?php get_template_part('template-parts/archive-profiles/grid'); ?>
    </main>
        
<?php get_footer(); ?>