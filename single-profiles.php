<?php get_header(); ?>
    
    <main class="site-content">
        <?php get_template_part('template-parts/single-profiles/back'); ?>

        <?php get_template_part('template-parts/single-profiles/profile'); ?>

        <?php get_template_part('template-parts/single-profiles/pagination'); ?>

        <?php get_template_part('template-parts/single-profiles/related'); ?>
    </main>
	
<?php get_footer(); ?>