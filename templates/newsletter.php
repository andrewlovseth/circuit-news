<?php

/*
 * Template Name: Newsletter
 * Template Post Type: post
 */

get_header(); ?>

	<section class="main">

		<?php get_template_part('template-parts/daily-kickoff/subscribe-banner'); ?>

		<article class="newsletter quick-hit page-content">
			<?php get_template_part('template-parts/daily-kickoff/recent-dropdown'); ?>

			<section class="header">
				<div class="header-wrapper">
					<div class="title">
						<h2 class="post-type">The Weekly Circuit</h2>
					</div>

					<div class="date">
						<span><?php the_time('F j, Y'); ?></span>
					</div>		

					<?php get_template_part('template-parts/article/share'); ?>
				</div>
			</section>

			<?php get_template_part('template-parts/article/body'); ?>

		</article>

		<?php get_template_part('template-parts/daily-kickoff/archive'); ?>
	</section>
	
<?php get_footer(); ?>