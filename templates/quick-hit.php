<?php

/*
 * Template Name: Quick Hit
 * Template Post Type: post
 */

get_header(); ?>

	<section class="main">
			
		<?php get_template_part('template-parts/global/sidebar'); ?>

		<article class="quick-hit page-content">

			<section class="header">
				<div class="header-wrapper">

					<?php get_template_part('template-parts/quick-hit/tagline'); ?>

					<?php get_template_part('template-parts/article/title'); ?>

					<?php get_template_part('template-parts/article/dek'); ?>

					<?php get_template_part('template-parts/quick-hit/photo'); ?>

					<?php get_template_part('template-parts/quick-hit/byline'); ?>

					<?php get_template_part('template-parts/article/share'); ?>

					<?php get_template_part('template-parts/article/dateline'); ?>		

				</div>
			</section>

			<?php get_template_part('template-parts/article/body'); ?>

		</article>

	</section>
	
<?php get_footer(); ?>