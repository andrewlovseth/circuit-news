<?php

/*

	Template Name: Home

*/

global $theme;
$theme = "overlay";

get_header(); ?>

	<?php get_template_part('template-parts/home/featured-articles'); ?>

	<section class="main">
			
		<?php get_template_part('template-parts/global/sidebar'); ?>

		<?php get_template_part('template-parts/global/subscribe'); ?>

	</section>

<?php get_footer(); ?>