<?php

/*

	Template Name: Subscribe

*/

get_header(); ?>

	<section class="main">
			
		<?php get_template_part('template-parts/global/subscribe'); ?>

	</section>

<?php get_footer(); ?>