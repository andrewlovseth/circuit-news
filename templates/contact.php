<?php

/*

	Template Name: Contact

*/

get_header(); ?>

	<?php get_template_part('template-parts/contact/page-header'); ?>

	<?php get_template_part('template-parts/contact/main'); ?>

<?php get_footer(); ?>